/*
    Getting started with SDL2 - Just a Window. 
*/



//For exit()
#include <stdlib.h>
#include <SDL.h>
#include "SDL_image.h"

int main( int argc, char* args[] )
{
    SDL_Window* gameWindow = nullptr;
    SDL_Renderer* gameRenderer = nullptr;
    SDL_Surface* temp = nullptr;
    SDL_Texture* texture;

    SDL_Init(SDL_INIT_EVERYTHING);

    gameWindow = SDL_CreateWindow("Hello my name is hooliganJosh", // window title
        SDL_WINDOWPOS_UNDEFINED, // xPosition
        SDL_WINDOWPOS_UNDEFINED, // yPosition
        800, 600, // window width, height
        SDL_WINDOW_SHOWN); // window flag? 


    // create renderer
    gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

    // load image
    temp = IMG_Load("assets/images/background.png");     

    texture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    SDL_FreeSurface(temp);

    // clears the screen
    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 69, 255); // R G B A 

    SDL_RenderClear(gameRenderer);

    SDL_RenderCopy(gameRenderer, texture, NULL, NULL);
 
    // draw current frame to screen
    SDL_RenderPresent(gameRenderer);

    // delays window close for some number of milliseconds
    SDL_Delay(10000);

    // cleaning up the mess
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(gameRenderer);
    SDL_DestroyWindow(gameWindow);

    // shuts down SDL, cleaning subroutines
    SDL_Quit();

    // Code zero = victory
    exit(0);
}