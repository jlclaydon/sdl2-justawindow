# DESCRIPTION #
This code sample creates a window and loads waits for 10 seconds and closes. This is the SDL2 version which uses hardware rendering from the outset. 

|Version|Status|
|:--|:-:|
|Linux|**DONE**|
|Windows|**DONE**| 

# INSTRUCTIONS #

At present I'm maintaining a version for Command Line Linux and Visual Studio 2019 only, if other versions are required let me know and I'll set them up.  

The build environment on Windows takes a bit of setting up if you want to do it all yourself, however, I will provide a video detailing the setup process for anyone who wants to setup their project from scratch. 

## Visual Studio 2019 ##

1. Open the project
2. Build
3. Debug
   
There should be no additional steps, a post-build process will copy the required dll files to the output directory and the debugger is set to run from there.  

## Linux ##

Make sure you have the these packages installed:
```
sdl2
sdl2-devel
sdl2_image
sdl2_image-devel
```

Package names may vary on other flavours of Linux (these are the Fedora ones). 

1. Open a terminal in the Linux folder (or via MS Code)
2. Compile ```g++ -o test -lSDL2 -lSDL2_image main.cpp```
3. Run ```./test```

Debugging via gdb is possible but painful, consider using an editor if required. 


 
